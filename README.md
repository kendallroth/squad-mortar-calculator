# Squad Mortar Calculator

> Simple mortar calculator for [Squad](https://joinsquad.com) that can quickly calculate bearing and milliradians for mortar targets.

<a href='https://play.google.com/store/apps/details?id=com.kendallroth.squad.mortarcalculator&hl=en_CA'>
  <img
    alt='Get it on Google Play'
    height="50"
    src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png'
  />
</a>

<div align="center">
  <img
    alt="Promotional Image - Main"
    height="350"
    src="assets/screenshots/0_3_0_promotional_image_calculation.png"
  />
</div>

## Features

- Built-in "phone" keypad to simplify entering coordinates
- Accurate to a sub-sub-sub coordinate (ie. "B12-6-2-3")
- Supports all map sizes with a standard grid (300m)
- Beautiful user interface and design

## Development

Contributions are welcome!

See the [Development Guide](DEVELOPMENT.md)

## Miscellaneous

Icon was taken from [Expo - Build Icon](https://buildicon.netlify.app/?color=ffc107&emoji=bomb)
