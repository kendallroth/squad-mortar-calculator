/**
 * Taken from: https://github.com/Endebert/squadmc/blob/dev/src/assets/Utils.js
 */

// Utilities
import { gridSize, mortarDistances, mortarMils } from "@constants";

const minDistance = mortarDistances[0];
const maxDistance = mortarDistances[mortarDistances.length - 1];

/**
 * Calculate mortar targeting stats
 *
 * @param   mortar - Mortar location
 * @param   target - Target location
 * @returns Mortar targeting stats
 */
export const calculateTarget = (
  mortar: GridLocation | null,
  target: GridLocation | null
): Calculation | null => {
  if (!mortar || !target) return null;

  const mortarPoint = getPoint(mortar);
  const targetPoint = getPoint(target);

  // Prevent calculations for the same point
  if (mortarPoint.x === targetPoint.x && mortarPoint.y === targetPoint.y) {
    return {
      azimuth: 0,
      distance: 0,
      milliradians: 0,
    };
  }

  const distance = Math.round(getDistance(mortarPoint, targetPoint));
  const azimuth = Math.round(getBearing(mortarPoint, targetPoint));
  const milliradians = Math.round(getMilliradians(distance));

  return {
    azimuth,
    distance,
    inRange: distance >= minDistance && distance <= maxDistance,
    milliradians,
  };
};

/**
 * Convert a map location to a point
 *
 * @param   location - Grid location
 * @returns Map point
 */
export const getPoint = (location: GridLocation): Point => {
  let x = 0;
  let y = 0;

  const letterCode = location.gridLetter.charCodeAt(0);
  const letterIndex = letterCode - 65;
  const numberIndex = location.gridNumber - 1;
  x += gridSize * letterIndex;
  y += gridSize * numberIndex;

  let subGridIdx = 0;
  while (subGridIdx < location.subGrids.length) {
    const subX = (location.subGrids[subGridIdx] - 1) % 3;
    const subY = 2 - (Math.ceil(location.subGrids[subGridIdx] / 3) - 1);
    const subGridSize = gridSize / 3 ** (subGridIdx + 1);

    x += subX * subGridSize;
    y += subY * subGridSize;

    subGridIdx++;
  }

  // At the end, add half of last interval, so it points to the center of the deepest sub-keypad
  const finalSubGridSize = gridSize / 3 ** subGridIdx;
  x += finalSubGridSize / 2;
  y += finalSubGridSize / 2;

  return { x, y };
};

/**
 * Calculates the distance between two points.
 *
 * @param   initial - Initial point
 * @param   target  - Target point
 * @returns Distance from initial point to target
 */
export const getDistance = (initial: Point, target: Point): number => {
  const distanceX = initial.x - target.x;
  const distanceY = initial.y - target.y;

  return Math.sqrt(distanceX * distanceX + distanceY * distanceY);
};

/**
 * Calculates the bearing required to see point B from point A.
 *
 * Adapted from: https://stackoverflow.com/questions/49760354/get-the-angle-degrees-from-2-points-on-a-grid
 *
 * @param   {Point} initial - Initial point
 * @param   {Point} target  - Target point
 * @returns {number} - Bearing from initial point to target
 */
export const getBearing = (initial: Point, target: Point): number => {
  const deltaX = target.x - initial.x;
  const deltaY = target.y - initial.y;

  // NOTE: Flip sign/order to account for re-mapping "normal" coordinate system
  let radians = Math.atan2(deltaX, -deltaY);
  if (radians < 0) {
    radians += 2 * Math.PI;
  }

  return (radians * 180) / Math.PI;
};

/**
 * Convert distance to milliradians (elevation marks)
 *
 * @param   distance - Distance to target
 * @returns Milliradians *elevation)
 */
export const getMilliradians = (distance: number): number => {
  if (distance < minDistance || distance > maxDistance) return 0;

  const mils = mortarMils;
  const distances = mortarDistances;

  for (let i = 0; i < mortarDistances.length; i++) {
    const value = mortarDistances[i];
    /* istanbul ignore next => Will almost never happen */
    if (value === distance) return mortarMils[i];

    // Distance does not convert strictly linearly to milliradians (must be interpolated)
    if (value > distance) {
      const mult = (mils[i] - mils[i - 1]) / (distances[i] - distances[i - 1]);
      return mult * (distance - distances[i]) + mils[i];
    }
  }

  /* istanbul ignore next => Should never happen */
  return 0;
};
