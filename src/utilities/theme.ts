import { DefaultTheme } from "react-native-paper";

export const colors = {
  ...DefaultTheme.colors,
  // Theme colors
  // accent: "#eb144c",
  background: "#f6f6f6",
  error: "#c62828",
  errorDark: "#b71c1c",
  primary: "#ffc107",
  primaryDark: "#ffa000",
  primaryDarkest: "#ff6f00",
  primaryLight: "#ffd54f",
  surface: "#ffffff",
  text: "#000000",
  // Named colors
  white: "#ffffff",
  black: "#000000",
  // Shades
  grey: "#757575",
  greyDark: "#424242",
  greyLight: "#bdbdbd",
  greyLightest: "#e0e0e0",
};

export default {
  ...DefaultTheme,
  colors,
};
