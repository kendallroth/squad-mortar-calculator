/** Cardinal directions */
export const cardinalDirections = ["N", "NE", "E", "SE", "S", "SW", "W", "NW"];

/** App developer email */
export const developerEmail = "kendall@kendallroth.ca";

/** App developer URL */
export const developerUrl = "https://www.kendallroth.ca";

export const gitlabUrl =
  "https://gitlab.com/kendallroth/squad-mortar-calculator";

/** Grid size (meters) */
export const gridSize = 300;

/** Mortar distances (maps to mils) */
// prettier-ignore
export const mortarDistances = [50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000, 1050, 1100, 1150, 1200, 1250];
/** Mortar milliradians (maps to distance) */
// prettier-ignore
export const mortarMils = [1579, 1558, 1538, 1517, 1496, 1475, 1453, 1431, 1409, 1387, 1364, 1341, 1317, 1292, 1267, 1240, 1212, 1183, 1152, 1118, 1081, 1039, 988, 918, 800];

/** Squad MC map calculator site */
export const squadMcUrl = "https://squadmc.ende.pro";

/** Number of sub-grids */
export const subGridCount = 3;
