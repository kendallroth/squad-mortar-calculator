/**
 * NOTE: Test values were calculated at https://squadmc.ende.pro
 *         - Mortar: B03-4-2
 *         - Target: C05-8-3
 */

// Utilities
import {
  calculateTarget,
  getBearing,
  getDistance,
  getMilliradians,
  getPoint,
} from "./trigonometry";

const mortarLocation: GridLocation = {
  gridLetter: "B",
  gridNumber: 3,
  subGrids: [4, 2],
};

const mortarTarget: GridLocation = {
  gridLetter: "C",
  gridNumber: 5,
  subGrids: [8, 3],
};

const mortarBearing = 139;
const mortarDistance = 662;
const mortarMilliradians = 1311;

describe("'getBearing'", () => {
  it("can get bearing between two points", () => {
    const origin = getPoint(mortarLocation);
    const target = getPoint(mortarTarget);

    // NOTE: Calculate inverse bearing as well to cover those lines of code (negative radian check)
    let bearing1 = getBearing(origin, target);
    bearing1 = Math.round(bearing1);
    let bearing2 = getBearing(target, origin);
    bearing2 = Math.round(bearing2);

    expect(bearing1).toEqual(mortarBearing);
    expect(bearing2).toEqual(319);
  });
});

describe("'getDistance'", () => {
  it("can get distance between two near points", () => {
    const origin = getPoint(mortarLocation);
    const target = getPoint(mortarTarget);

    let distance = getDistance(origin, target);
    distance = Math.round(distance);

    expect(distance).toEqual(mortarDistance);
  });

  it("can get distance between two far points", () => {
    const origin = getPoint({ gridLetter: "A", gridNumber: 7, subGrids: [5] });
    const target = getPoint({ gridLetter: "Z", gridNumber: 3, subGrids: [5] });

    let distance = getDistance(origin, target);
    distance = Math.round(distance);

    expect(distance).toEqual(7595);
  });

  it("can handle same point", () => {
    const origin = getPoint(mortarLocation);
    const target = getPoint(mortarLocation);

    const distance = getDistance(origin, target);

    expect(distance).toEqual(0);
  });
});

describe("'getMilliradians'", () => {
  it("can get milliradians from distance", () => {
    const origin = getPoint(mortarLocation);
    const target = getPoint(mortarTarget);
    const distance = getDistance(origin, target);

    let mils = getMilliradians(distance);
    mils = Math.round(mils);

    expect(mils).toEqual(mortarMilliradians);
  });

  // TODO: Support out-of-range (max and min)
});

describe("'getPoint'", () => {
  it("can convert location to point", () => {
    let point = getPoint(mortarLocation);
    point = { x: Math.round(point.x), y: Math.round(point.y) };

    const expectedPoint: Point = { x: 350, y: 783 };
    expect(point).toEqual(expectedPoint);
  });
});

describe("'calculateTarget", () => {
  it("can calculate targeteting statistics", () => {
    const stats = calculateTarget(mortarLocation, mortarTarget);

    const expectedStats = {
      azimuth: 139,
      distance: 662,
      inRange: true,
      milliradians: 1311,
    };
    expect(stats).toEqual(expectedStats);
  });

  it("can handle same locations", () => {
    const stats = calculateTarget(mortarLocation, mortarLocation);

    const expectedStats = {
      azimuth: 0,
      distance: 0,
      milliradians: 0,
    };
    expect(stats).toEqual(expectedStats);
  });

  it("can handle too far apart locations", () => {
    const farLocation = { gridLetter: "Z", gridNumber: 3, subGrids: [2] };

    const stats = calculateTarget(mortarLocation, farLocation);

    const expectedStats = {
      azimuth: 91,
      distance: 7300,
      inRange: false,
      milliradians: 0,
    };
    expect(stats).toEqual(expectedStats);
  });

  it("can handle missing location", () => {
    const emptyStats1 = calculateTarget(null, mortarTarget);
    const emptyStats2 = calculateTarget(mortarLocation, null);

    expect(emptyStats1).toEqual(null);
    expect(emptyStats2).toEqual(null);
  });
});
