import React from "react";
import { Image, StatusBar, StyleSheet } from "react-native";
import { Appbar as BaseAppBar } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";

type AppBarProps = {
  back?: boolean;
  children?: React.ReactNode;
  logo?: boolean;
  subtitle?: string;
  title: string;
};

const AppBar = (props: AppBarProps): React.ReactElement => {
  const { back = true, children, logo = false, subtitle, title } = props;

  const navigation = useNavigation();

  return (
    <BaseAppBar.Header
      statusBarHeight={StatusBar.currentHeight}
      style={styles.header}
    >
      {back && <BaseAppBar.BackAction onPress={navigation.goBack} />}
      {logo && (
        <Image
          fadeDuration={100}
          resizeMode="contain"
          source={require("@assets/icons/logo.png")}
          style={styles.headerLogo}
        />
      )}
      <BaseAppBar.Content subtitle={subtitle} title={title} />
      {children}
    </BaseAppBar.Header>
  );
};

const styles = StyleSheet.create({
  header: {
    // NOTE: Disabling shadow is currently not necessary
    // elevation: 0,
    // shadowOpacity: 0,
  },
  headerLogo: {
    width: 32,
    height: 32,
    marginLeft: 8,
  },
});

AppBar.Action = BaseAppBar.Action;

export default AppBar;
