import React, { useState } from "react";
import { LayoutChangeEvent, StyleSheet, View } from "react-native";

// Components
import { MonoText } from "@components/Typography";
import KeypadKey from "./KeypadKey";

// Utilities
import { colors } from "@theme";

const keypad: KeypadKey[][] = [
  [
    { letters: ["A", "B", "C"], number: 1 },
    { letters: ["D", "E", "F"], number: 2 },
    { letters: ["G", "H", "I"], number: 3 },
  ],
  [
    { letters: ["J", "K", "L"], number: 4 },
    { letters: ["M", "N", "O"], number: 5 },
    { letters: ["P", "Q", "R"], number: 6 },
  ],
  [
    { letters: ["S", "T", "U"], number: 7 },
    { letters: ["V", "W", "X"], number: 8 },
    { letters: ["Y", "Z"], number: 9 },
  ],
];

type KeypadPhoneProps = {
  /** Whether keypad value can be submitted */
  submit: boolean;
  /** Keypad mode */
  mode?: "letters" | "numbers" | "numbersNonZero" | null;
  // location: GridLocation | null;
  onInput: (key: KeypadKey) => void;
  onMoveNext: () => void;
  onMovePrevious: () => void;
  onSubmit?: () => void;
};

const KeypadPhone = (props: KeypadPhoneProps): React.ReactElement => {
  const {
    mode,
    submit = false,
    onInput,
    onMoveNext,
    onMovePrevious,
    onSubmit,
  } = props;

  const [keypadWidth, setKeypadWidth] = useState(1);

  const itemSize = (keypadWidth - keypadPadding * 2) / 4;
  const keyStyleHighlight = { color: colors.primaryLight };
  const keyStyleNormal = { color: colors.greyLight };

  function onLayoutChange(event: LayoutChangeEvent) {
    const nativeEvent = event.nativeEvent;
    const { width } = nativeEvent.layout;

    setKeypadWidth(width);
  }

  const goToNextField = () => {
    onMoveNext();
  };

  const goToPreviousField = () => {
    onMovePrevious();
  };

  const handleChange = (key: KeypadKey) => {
    onInput(key);
  };

  const handleSubmit = () => {
    onSubmit && onSubmit();
  };

  return (
    <View style={styles.keypad} onLayout={onLayoutChange}>
      {keypad.map((row, idx) => (
        <View key={idx} style={styles.keypadRow}>
          {row.map((key) => {
            return (
              <KeypadKey
                key={key.number}
                sizeStyle={{ height: itemSize }}
                onPress={() => handleChange(key)}
              >
                <View style={styles.keypadKey}>
                  <MonoText
                    style={[
                      styles.keypadKeyNumber,
                      mode === "numbers" || mode === "numbersNonZero"
                        ? keyStyleHighlight
                        : keyStyleNormal,
                    ]}
                  >
                    {key.number}
                  </MonoText>
                  <MonoText
                    style={[
                      styles.keypadKeyLetters,
                      mode === "letters" ? keyStyleHighlight : keyStyleNormal,
                    ]}
                  >
                    {key.letters.join("")}
                  </MonoText>
                </View>
              </KeypadKey>
            );
          })}
        </View>
      ))}
      <View key="bottomRow" style={styles.keypadRow}>
        <KeypadKey
          icon="arrow-left"
          sizeStyle={{ height: itemSize }}
          onPress={goToPreviousField}
        />
        {submit ? (
          <KeypadKey
            sizeStyle={{ height: itemSize }}
            icon="check"
            iconStyle={{ color: colors.primary }}
            onPress={handleSubmit}
          />
        ) : (
          <KeypadKey
            sizeStyle={{ height: itemSize }}
            value={0}
            valueStyle={mode === "numbers" ? keyStyleHighlight : keyStyleNormal}
            onPress={() => handleChange({ letters: [], number: 0 })}
          />
        )}
        <KeypadKey
          icon="arrow-right"
          sizeStyle={{ height: itemSize }}
          onPress={goToNextField}
        />
      </View>
    </View>
  );
};

const keypadPadding = 20;

const styles = StyleSheet.create({
  keypad: {
    alignItems: "center",
    padding: keypadPadding,
    backgroundColor: colors.greyDark,
    borderTopLeftRadius: 35,
    borderTopRightRadius: 35,
  },
  keypadKey: {
    alignItems: "center",
    justifyContent: "center",
  },
  keypadKeyNumber: {
    fontSize: 30,
    fontWeight: "700",
  },
  keypadKeyLetters: {
    fontSize: 20,
    letterSpacing: 4,
  },
  keypadRow: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
});

export default KeypadPhone;
