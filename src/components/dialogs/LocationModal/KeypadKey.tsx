import React from "react";
import { StyleSheet, TextStyle, View, ViewStyle } from "react-native";
import { TouchableRipple } from "react-native-paper";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

// Components
import { MonoText } from "@components/Typography";

// Utilities
import { colors } from "@theme";

type KeypadKeyProps = {
  /** Whether location input is active */
  active?: boolean;
  /** Input children (if not using 'value') */
  children?: React.ReactNode;
  /** Whether key is disabled */
  disabled?: boolean;
  /** Key icon */
  icon?: string;
  /** Key icon style */
  iconStyle?: TextStyle;
  /** Size style object */
  sizeStyle: ViewStyle;
  /** Style object */
  style?: ViewStyle;
  /** Key value */
  value?: string | number;
  /** Value style object */
  valueStyle?: TextStyle;
  onPress: () => void;
};

const renderChildren = (props: KeypadKeyProps): React.ReactNode | null => {
  const { children, icon, iconStyle = {}, value, valueStyle = {} } = props;

  if (children) return children;

  if (icon) {
    return (
      <Icon name={icon} size={30} style={[styles.keypadKeyIcon, iconStyle]} />
    );
  }

  if (value !== undefined && value !== null) {
    return (
      <MonoText style={[styles.keypadKeyText, valueStyle]}>{value}</MonoText>
    );
  }
};

const KeypadKey = (props: KeypadKeyProps): React.ReactElement => {
  const { disabled = false, sizeStyle, style = {}, onPress } = props;

  return (
    <View
      style={[
        styles.keypadKey,
        disabled ? styles.keypadKeyDisabled : null,
        sizeStyle,
      ]}
    >
      <TouchableRipple
        borderless
        disabled={disabled}
        rippleColor="#716233"
        style={[styles.keypadKeyRipple, style]}
        onPress={onPress}
      >
        {renderChildren(props)}
      </TouchableRipple>
    </View>
  );
};

const styles = StyleSheet.create({
  keypadKey: {
    // NOTE: Width is set by parent (based on columns)
    aspectRatio: 1,
    padding: 5,
  },
  keypadKeyDisabled: {
    opacity: 0.4,
  },
  keypadKeyIcon: {
    alignSelf: "center",
    color: "white",
  },
  keypadKeyRipple: {
    flexGrow: 1,
    justifyContent: "center",
    alignContent: "center",
    backgroundColor: "#ffffff11",
    borderRadius: 25,
  },
  keypadKeyText: {
    fontSize: 30,
    textAlign: "center",
    color: colors.white,
  },
  keypadKeyTextActive: {
    color: colors.primary,
  },
});

export default KeypadKey;
