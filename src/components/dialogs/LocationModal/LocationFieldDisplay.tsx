import React from "react";
import { StyleSheet, View, ViewStyle } from "react-native";
import { TouchableRipple } from "react-native-paper";

// Components
import { MonoText } from "@components/Typography";

// Utilities
import { colors } from "@theme";

type LocationFieldDisplayProps = {
  /** Whether location field is active */
  active: boolean;
  /** Container style */
  containerStyle?: ViewStyle;
  /** Whether field is large (double digits) */
  large?: boolean;
  /** Style object */
  style?: ViewStyle;
  /** Location field value */
  value: string | number;
  onPress?: () => void;
};

const LocationFieldDisplay = (
  props: LocationFieldDisplayProps
): React.ReactElement => {
  const {
    active,
    containerStyle,
    large = false,
    style = {},
    value,
    onPress,
  } = props;

  return (
    <View
      style={[
        styles.locationDisplayValue,
        large ? { aspectRatio: 1.5 } : {},
        containerStyle,
        active ? styles.locationDisplayValueActive : {},
      ]}
    >
      <TouchableRipple
        borderless
        style={[styles.locationDisplayRipple, style]}
        onPress={onPress}
      >
        <MonoText style={styles.locationDisplayText}>{value}</MonoText>
      </TouchableRipple>
    </View>
  );
};

const minDisplayValueSize = 50;

const styles = StyleSheet.create({
  locationDisplayText: {
    fontSize: 30,
    textAlign: "center",
  },
  locationDisplayRipple: {
    justifyContent: "center",
    alignContent: "center",
    flexGrow: 1,
    backgroundColor: colors.background,
    borderRadius: 8,
  },
  locationDisplayValue: {
    height: minDisplayValueSize,
    aspectRatio: 1,
    borderWidth: 2,
    borderColor: colors.greyLight,
    borderRadius: 10,
  },
  locationDisplayValueActive: {
    borderColor: colors.primary,
    elevation: 3,
  },
});

export default LocationFieldDisplay;
