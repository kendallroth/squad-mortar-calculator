import React, { useEffect, useState } from "react";
import { StyleSheet, View } from "react-native";

// Components
import { MonoText } from "@components/Typography";
import BottomModal from "../BottomModal";
import KeypadPhone from "./KeypadPhone";
import LocationFieldDisplay from "./LocationFieldDisplay";

// Utilities
import { subGridCount } from "@constants";
import { colors } from "@theme";

const stepOrder = ["gridLetter", "gridNumber", "subGrids"];

const emptyLocation: EmptyGridLocation = {
  gridLetter: null,
  gridNumber: null,
  subGrids: [],
};

type LocationModalProps = {
  location: GridLocation | null;
  title: string;
  visible: boolean;
  onChange: (location: GridLocation) => void;
  onDismiss: () => void;
};

const LocationModal = (props: LocationModalProps): React.ReactElement => {
  const {
    location: initialLocation,
    title,
    visible,
    onChange,
    onDismiss,
  } = props;

  const [location, setLocation] = useState<GridLocation | EmptyGridLocation>(
    emptyLocation
  );
  const [fieldStep, setFieldStep] = useState("gridLetter");
  const [subGridStep, setSubGridStep] = useState(0);

  const canBackspace = false;
  const subGridRange = Array(subGridCount)
    .fill("")
    .map((_, i) => i);

  // Reset the modal when opening
  useEffect(() => {
    if (!visible) return;

    setFieldStep("gridLetter");
    setSubGridStep(0);

    // Locations can be quickly edited by selecting them
    if (initialLocation) {
      setLocation(initialLocation);
    } else {
      setLocation({ ...emptyLocation, subGrids: [] });
    }
  }, [visible]);

  const steps = {
    isGridLetter: fieldStep === "gridLetter",
    isGridNumber: fieldStep === "gridNumber",
    isSubGrid: fieldStep === "subGrids",
  };

  const canSubmit = Boolean(
    steps.isSubGrid && location.subGrids[0] && location.subGrids[1]
  );

  let keypadMode = "letters";
  if (steps.isGridNumber) keypadMode = "numbers";
  if (steps.isSubGrid) keypadMode = "numbersNonZero";

  /**
   * Dismiss the location input modal
   *
   * Will submit the location if valid
   */
  const handleDismiss = () => {
    // Submit location if valid (at least one sub-grid)
    if (location.gridLetter && location.gridNumber && location.subGrids[0]) {
      handleSubmit();
    }

    onDismiss();
  };

  const handleDisplayPress = (display: string, subGridIdx?: number) => {
    if (display === "gridLetter") {
      setFieldStep("gridLetter");
    } else if (display === "subGrids") {
      if (subGridIdx !== undefined && subGridIdx < subGridCount) {
        if (location.subGrids[subGridIdx]) {
          setFieldStep("subGrids");
          setSubGridStep(subGridIdx);
        }
      }
    }
  };

  /**
   * Handle keyboard input (according to current field)
   *
   * @param key - Selected key
   */
  const handleInput = (key: KeypadKey) => {
    const newLocation = { ...location };

    if (steps.isGridLetter) {
      // Constrain next letter within letters available
      let nextLetterIdx = key.letters.indexOf(location.gridLetter || "") + 1;
      nextLetterIdx = nextLetterIdx >= 0 ? nextLetterIdx : 0;
      nextLetterIdx = nextLetterIdx < key.letters.length ? nextLetterIdx : 0;

      newLocation.gridLetter = key.letters[nextLetterIdx];
    } else if (steps.isGridNumber) {
      if (newLocation.gridNumber !== null && newLocation.gridNumber < 10) {
        newLocation.gridNumber = newLocation.gridNumber * 10 + key.number;
      } else {
        newLocation.gridNumber = key.number;
      }
    } else if (steps.isSubGrid) {
      newLocation.subGrids[subGridStep] = key.number;
    }

    setLocation(newLocation);
  };

  /**
   * Move to the next field
   *
   * Submits the location if all fields have been entered
   */
  const handleMoveNext = () => {
    const currentStepIdx = stepOrder.findIndex((s) => s === fieldStep);
    const hasNextStep = currentStepIdx < stepOrder.length;
    const nextStep = hasNextStep ? stepOrder[currentStepIdx + 1] : null;

    // Prevent skipping grid letter or number
    if (steps.isGridLetter && !location.gridLetter) return;
    if (steps.isGridNumber && location.gridNumber === null) return;

    if (nextStep) {
      setFieldStep(nextStep);

      if (nextStep === "subGrids") {
        setSubGridStep(0);
      }

      return;
    }

    // Prevent skipping current sub-grid number
    if (steps.isSubGrid && !location.subGrids[subGridStep]) return;

    const hasNextSubGrid = subGridStep < subGridCount - 1;
    const nextSubGrid = hasNextSubGrid ? subGridStep + 1 : null;

    if (nextSubGrid) {
      setSubGridStep(nextSubGrid);

      return;
    }

    // Submit form on when pressing "next" on last subgrid value
    handleSubmit();
  };

  /**
   * Move to the previous field (to overwrite it)
   *
   * Backspacing is optional (disabled by default)
   */
  const handleMovePrevious = () => {
    if (steps.isGridLetter) {
      canBackspace && setLocation({ ...location, gridLetter: null });
      return;
    }

    if (steps.isGridNumber) {
      setFieldStep("gridLetter");
      canBackspace && setLocation({ ...location, gridNumber: null });
      return;
    }

    const hasPreviousSubGrid = subGridStep > 0;
    const previousSubGrid = hasPreviousSubGrid ? subGridStep - 1 : null;

    if (previousSubGrid !== null) {
      canBackspace &&
        setLocation({
          ...location,
          subGrids: location.subGrids.slice(0, subGridStep),
        });
      setSubGridStep(previousSubGrid);
    } else {
      setFieldStep("gridNumber");
      canBackspace && setLocation({ ...location, subGrids: [] });
    }
  };

  /**
   * Handle submitting the entered location
   */
  const handleSubmit = () => {
    onChange(location as GridLocation);
  };

  return (
    <BottomModal
      dismissable
      title={title}
      visible={visible}
      onDismiss={handleDismiss}
    >
      <View style={styles.locationDisplay}>
        <LocationFieldDisplay
          active={steps.isGridLetter || steps.isGridNumber}
          large
          value={`${location.gridLetter?.toUpperCase() || ""}${
            location.gridNumber || ""
          }`}
          onPress={() => handleDisplayPress("gridLetter")}
        />
        {subGridRange.map((idx) => [
          <MonoText key="divider" style={styles.locationDisplaySeparator}>
            &mdash;
          </MonoText>,
          <LocationFieldDisplay
            key={`field${idx}`}
            active={steps.isSubGrid && subGridStep === idx}
            containerStyle={idx > 1 ? { borderColor: colors.greyLightest } : {}}
            value={location.subGrids[idx] || ""}
            onPress={() => handleDisplayPress("subGrids", idx)}
          />,
        ])}
      </View>
      <KeypadPhone
        // @ts-ignore
        mode={keypadMode}
        submit={canSubmit}
        onInput={handleInput}
        onMoveNext={handleMoveNext}
        onMovePrevious={handleMovePrevious}
        onSubmit={handleSubmit}
      />
    </BottomModal>
  );
};

const minDisplayValueSize = 45;

const styles = StyleSheet.create({
  locationDisplay: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 25,
  },
  locationDisplaySeparator: {
    marginHorizontal: 5,
    fontSize: 24,
  },
  locationDisplayValue: {
    height: minDisplayValueSize,
    justifyContent: "center",
    alignContent: "center",
    minWidth: minDisplayValueSize,
    backgroundColor: colors.background,
    borderWidth: 2,
    borderColor: colors.primary,
    borderRadius: 10,
  },
  locationDisplayValueLarge: {
    paddingHorizontal: 10,
  },
  locationKeyboard: {
    flexDirection: "row",
    flexWrap: "wrap",
    padding: 20,
    backgroundColor: colors.greyDark,
    borderTopLeftRadius: 35,
    borderTopRightRadius: 35,
  },
});

export default LocationModal;
