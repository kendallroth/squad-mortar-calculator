import React from "react";
import { Surface } from "react-native-paper";
import { StyleSheet, Text, View } from "react-native";
import Modal from "react-native-modal";

type BottomModalProps = {
  children: React.ReactNode;
  dismissable?: boolean;
  title: string;
  visible: boolean;
  onDismiss: () => void;
};

const BottomModal = (props: BottomModalProps): React.ReactElement => {
  const { children, dismissable = false, title, visible, onDismiss } = props;

  const onDismissAttempt = () => {
    if (!dismissable) return;

    onDismiss && onDismiss();
  };

  return (
    <Modal
      backdropOpacity={0.4}
      // Fix flickering exit animation (https://github.com/react-native-modal/react-native-modal#the-modal-enterexit-animation-flickers)
      backdropTransitionOutTiming={0}
      isVisible={visible}
      style={styles.modalBackdrop}
      onBackButtonPress={onDismissAttempt}
      onBackdropPress={onDismissAttempt}
    >
      <Surface style={styles.modal}>
        <Text style={styles.modalTitle}>{title}</Text>
        <View style={styles.modalContent}>{children}</View>
      </Surface>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalBackdrop: {
    flexGrow: 1,
    justifyContent: "flex-end",
    margin: 0,
  },
  modal: {
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  modalContent: {},
  modalTitle: {
    padding: 10,
    fontSize: 18,
    textAlign: "center",
    fontWeight: "bold",
  },
});

export default BottomModal;
