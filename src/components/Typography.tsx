import React from "react";
import { Platform, Text, TextStyle } from "react-native";

type MonoTextProps = {
  /** Text for monospacing */
  children: React.ReactNode;
  /** Text style */
  style?: TextStyle | TextStyle[];
};

/**
 * Monospaced text
 */
const MonoText = (props: MonoTextProps): React.ReactElement => {
  const { children, style } = props;

  const fontFamily = Platform.OS === "ios" ? "Courier" : "monospace";

  return <Text style={[{ fontFamily }, style]}>{children}</Text>;
};

export { MonoText };
