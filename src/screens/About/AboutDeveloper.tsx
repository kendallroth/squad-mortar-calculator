import React from "react";
import * as Linking from "expo-linking";
import { StyleSheet, View, ViewStyle } from "react-native";
import { Chip, Text } from "react-native-paper";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

// Utilities
import { developerEmail, developerUrl, gitlabUrl } from "@constants";
import { colors } from "@theme";

const developerActions = [
  { icon: "account", name: "Portfolio", url: developerUrl },
  { icon: "gitlab", name: "Repository", url: gitlabUrl },
  { icon: "react", name: "Technology", url: "https://reactnative.dev/" },
  { icon: "email", name: "Contact", url: `mailto:${developerEmail}` },
];

type AboutFooterProps = {
  /** Style object */
  style?: ViewStyle;
};

const AboutDeveloper = (props: AboutFooterProps): React.ReactElement | null => {
  const { style } = props;

  const onLink = (url: string) => {
    Linking.openURL(url);
  };

  return (
    <View style={[styles.info, style]}>
      <Text style={styles.infoText}>Developed by Kendall Roth &copy; 2020</Text>
      <View style={styles.infoActions}>
        {developerActions.map((action) => (
          <Chip
            key={action.name}
            icon={(iconProps) => (
              <Icon {...iconProps} name={action.icon} size={24} />
            )}
            style={styles.infoAction}
            textStyle={styles.infoActionText}
            onPress={() => onLink(action.url)}
          >
            {action.name}
          </Chip>
        ))}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  info: {},
  infoActions: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginTop: 15,
  },
  infoAction: {
    marginBottom: 10,
    marginRight: 10,
    backgroundColor: colors.primaryLight,
  },
  infoActionText: {
    fontSize: 18,
  },
  infoText: {
    fontSize: 16,
  },
});

export default AboutDeveloper;
