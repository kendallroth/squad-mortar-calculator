import React from "react";
import * as Linking from "expo-linking";
import { ScrollView, StyleSheet, View } from "react-native";
import { Button, Text, Title } from "react-native-paper";

// Components
import { AppBar } from "@components/layout";
import AboutDeveloper from "./AboutDeveloper";

// Utilities
import { squadMcUrl } from "@constants";
import { colors } from "@theme";

const AboutScreen = (): React.ReactElement | null => {
  const openSquadMcLink = () => {
    Linking.openURL(squadMcUrl);
  };

  return (
    <View style={styles.page}>
      <AppBar title="About" />
      <ScrollView style={styles.pageContent}>
        <Text style={styles.infoDescription}>
          Squad Mortar Calculator enables players to quickly range mortar
          targets. This can save critical time in the game (freeing up squad
          leader) and provides a much more accurate ranging than observe marks.
        </Text>
        <Text style={styles.infoDescription}>
          Additionally, a simple calculator is more immersive than some of the
          excellent map-based mortar calculation tools that already exist. It
          seeks to simulate the act of measuring between points on a map (via
          coordinates), vs a more electronic approach.
        </Text>
        <Text
          style={[styles.infoDescription, styles.infoDescriptionAdditional]}
        >
          Checkout SquadMC for a map-based approach!
        </Text>
        <Button
          color={colors.primaryDarkest}
          icon="open-in-new"
          mode="text"
          style={styles.infoDescriptionButton}
          onPress={openSquadMcLink}
        >
          SquadMC
        </Button>
        <Title style={{ marginTop: 25 }}>Developer</Title>
        <AboutDeveloper />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  infoDescription: {
    marginBottom: 10,
    fontSize: 16,
  },
  infoDescriptionAdditional: {
    marginTop: 10,
    color: colors.grey,
  },
  infoDescriptionButton: {
    alignSelf: "flex-start",
  },
  page: {
    flex: 1,
  },
  pageContent: {
    padding: 20,
  },
});

export default AboutScreen;
