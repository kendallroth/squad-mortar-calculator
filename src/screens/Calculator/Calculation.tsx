import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { Surface } from "react-native-paper";

// Utilities
import { colors } from "@theme";

type CalculationProps = {
  /** Calculation */
  calculation: Calculation | null;
};

const DividerVertical = () => (
  <View
    style={{ backgroundColor: colors.greyLightest, width: 1, height: "100%" }}
  />
);

const Calculation = (props: CalculationProps): React.ReactElement => {
  const { calculation } = props;

  const inRange = calculation ? calculation.inRange : true;
  const elevationIconColor = inRange ? colors.primaryDark : colors.error;
  const elevationTextStyle = !inRange
    ? { color: colors.errorDark }
    : { color: colors.black };

  const azimuthIconStyle =
    calculation && calculation.azimuth
      ? { transform: [{ rotateZ: `${calculation.azimuth}deg` }] }
      : {};
  const milliradianDisplay =
    calculation && inRange ? calculation.milliradians : "—";

  // NOTE: Can optionally show direction
  const showDirection = calculation && false;

  return (
    <Surface style={styles.calculation}>
      <View style={styles.calculationItem}>
        <Icon color={colors.primaryDark} name="compass-outline" size={40} />
        {showDirection && (
          <Icon
            color={colors.greyLight}
            name="navigation"
            size={20}
            style={[styles.azimuthIndicator, azimuthIconStyle]}
          />
        )}
        <Text style={styles.calculationItemText}>
          {calculation ? `${calculation.azimuth}°` : "—"}
        </Text>
        <Text style={styles.calculationItemDescription}>Azimuth</Text>
      </View>
      <DividerVertical />
      <View style={styles.calculationItem}>
        <Icon color={elevationIconColor} name="angle-acute" size={40} />
        <Text style={[styles.calculationItemText, elevationTextStyle]}>
          {milliradianDisplay}
        </Text>
        <Text style={styles.calculationItemDescription}>Milliradians</Text>
      </View>
      <DividerVertical />
      <View style={styles.calculationItem}>
        <Icon color={elevationIconColor} name="ruler" size={40} />
        <Text style={[styles.calculationItemText, elevationTextStyle]}>
          {calculation ? `${calculation.distance}m` : "—"}
        </Text>
        <Text style={styles.calculationItemDescription}>Distance</Text>
      </View>
    </Surface>
  );
};

const styles = StyleSheet.create({
  azimuthIndicator: {
    position: "absolute",
    right: 8,
    top: 8,
  },
  calculation: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 25,
    borderRadius: 8,
    elevation: 2,
  },
  calculationItem: {
    width: "33.33%",
    aspectRatio: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  calculationItemDescription: {
    fontSize: 14,
    color: colors.grey,
  },
  calculationItemText: {
    fontSize: 25,
    width: "100%",
    lineHeight: 35,
    fontWeight: "bold",
    textAlign: "center",
  },
});

export default Calculation;
