import React, { useMemo, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Divider, Surface } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";

// Components
import { LocationModal } from "@components/dialogs";
import { AppBar } from "@components/layout";
import Calculation from "./Calculation";
import LocationItem from "./LocationItem";

// Utilities
import { colors } from "@theme";
import { calculateTarget } from "@utilities/trigonometry";

const Calculator = (): React.ReactElement => {
  const [isEditMortarDialogShown, setIsEditMortarDialogShown] = useState(false);
  const [isEditTargetDialogShown, setIsEditTargetDialogShown] = useState(false);
  const [mortarLocation, setMortarLocation] = useState<GridLocation | null>(
    null
  );
  const [targetLocation, setTargetLocation] = useState<GridLocation | null>(
    null
  );

  const calculation = useMemo(
    () => calculateTarget(mortarLocation, targetLocation),
    [mortarLocation, targetLocation]
  );
  const navigation = useNavigation();

  const isMissingLocation = !mortarLocation || !targetLocation;
  const isSameLocation = calculation ? !calculation.distance : false;

  const setMortar = (location: GridLocation) => {
    setMortarLocation(location);

    // NOTE: Slight delay allows field to properly fill before modal animates
    setTimeout(() => {
      setIsEditMortarDialogShown(false);
    }, 100);
  };

  const setTarget = (location: GridLocation) => {
    setTargetLocation(location);

    // NOTE: Slight delay allows field to properly fill before modal animates
    setTimeout(() => {
      setIsEditTargetDialogShown(false);
    }, 100);
  };

  const onInfoPress = () => {
    navigation.navigate("About");
  };

  return (
    <View style={styles.page}>
      <AppBar back={false} logo title="Squad Mortar Calculator">
        <AppBar.Action icon="information" onPress={onInfoPress} />
      </AppBar>
      <LocationModal
        location={mortarLocation}
        title="Mortar Location"
        visible={isEditMortarDialogShown}
        onChange={setMortar}
        onDismiss={() => setIsEditMortarDialogShown(false)}
      />
      <LocationModal
        location={targetLocation}
        title="Target Location"
        visible={isEditTargetDialogShown}
        onChange={setTarget}
        onDismiss={() => setIsEditTargetDialogShown(false)}
      />
      <View style={styles.pageContent}>
        <Surface style={[styles.locations, { marginTop: 10 }]}>
          <LocationItem
            location={mortarLocation}
            type="Mortar:"
            onClearPress={() => setMortarLocation(null)}
            onEditPress={() => setIsEditMortarDialogShown(true)}
          />
          <Divider style={{ backgroundColor: colors.grey }} />
          <LocationItem
            location={targetLocation}
            type="Target:"
            onClearPress={() => setTargetLocation(null)}
            onEditPress={() => setIsEditTargetDialogShown(true)}
          />
        </Surface>
        {isMissingLocation && (
          <Text style={styles.calculationPrompt}>
            Missing location for calculation
          </Text>
        )}
        {isSameLocation && (
          <Text style={styles.calculationPrompt}>Locations are the same</Text>
        )}
        <Calculation calculation={calculation} />
        {calculation && (
          <Text style={styles.calculationWarning}>
            Remember that these values are estimations only!
          </Text>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  calculation: {
    marginTop: 50,
  },
  calculationPrompt: {
    marginTop: 25,
    padding: 10,
    fontSize: 16,
    textAlign: "center",
    color: colors.black,
    backgroundColor: colors.primaryLight,
    overflow: "hidden",
    borderRadius: 8,
  },
  calculationWarning: {
    marginTop: 25,
    fontSize: 14,
    lineHeight: 20,
    textAlign: "center",
    color: colors.grey,
  },
  locations: {
    borderRadius: 10,
    elevation: 2,
  },
  page: {
    flex: 1,
  },
  pageContent: {
    padding: 20,
  },
});

export default Calculator;
