import { colors } from "@theme";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { IconButton } from "react-native-paper";

// Components
import { MonoText } from "@components/Typography";

type LocationItemProps = {
  /** Grid location */
  location: GridLocation | null;
  /** Location type */
  type: string;
  onClearPress: () => void;
  onEditPress: () => void;
};

const LocationItem = (props: LocationItemProps): React.ReactElement => {
  const { location, type, onClearPress, onEditPress } = props;

  // NOTE: "Duplicate" condition is somehow necessary due to weird JSX bug, where
  //         clearing the field and setting again would not update the display...
  return (
    <View style={styles.location}>
      <Text style={styles.locationName}>{type}</Text>
      {location && (
        <MonoText style={styles.locationValue}>
          {location.gridLetter}
          {location.gridNumber}
          {location.subGrids.map((s) => `—${s}`)}
        </MonoText>
      )}
      {!location && (
        <Text style={[styles.locationValue, styles.locationValueEmpty]}>
          No location
        </Text>
      )}
      <View style={styles.locationActions}>
        <IconButton icon="close" onPress={onClearPress} />
        <IconButton icon="pencil" onPress={onEditPress} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  location: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 5,
    paddingHorizontal: 10,
  },
  locationActions: {
    flexDirection: "row",
    marginLeft: "auto",
  },
  locationName: {
    width: 80,
    fontSize: 18,
    fontWeight: "500",
  },
  locationValue: {
    fontSize: 20,
  },
  locationValueEmpty: {
    fontSize: 18,
    color: colors.grey,
  },
});

export default LocationItem;
