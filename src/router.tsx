import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

// Screens
import CalculatorScreen from "@screens/Calculator";
import AboutScreen from "@screens/About";

const Stack = createStackNavigator();

const Router = (): React.ReactElement => (
  <NavigationContainer>
    <Stack.Navigator
      initialRouteName="Calculator"
      screenOptions={{ headerShown: false }}
    >
      <Stack.Screen component={CalculatorScreen} name="Calculator" />
      <Stack.Screen component={AboutScreen} name="About" />
    </Stack.Navigator>
  </NavigationContainer>
);

export default Router;
