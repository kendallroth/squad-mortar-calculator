declare interface KeypadKey {
  letters: string[];
  number: number;
}
