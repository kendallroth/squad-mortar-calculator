declare interface Calculation {
  azimuth: number;
  distance: number;
  inRange?: boolean;
  milliradians: number;
}
