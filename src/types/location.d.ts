declare interface GridLocation {
  gridLetter: string;
  gridNumber: number;
  subGrids: number[];
}

declare interface EmptyGridLocation extends GridLocation {
  gridLetter: string | null;
  gridNumber: number | null;
  subGrids: number[];
}

declare interface Point {
  x: number;
  y: number;
}
